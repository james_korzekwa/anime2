from __future__ import absolute_import
import unittest

from anime.core.rubberband import RubberBand
import anime.core.filter as filter
from anime.core.pipe import Pipe
from itertools import ifilter

class RubberTestClass(RubberBand):

    def __init__(self):
        super(RubberTestClass, self).__init__()
        self.addition = 1
        self.x = 0
        self.y = 0
        self.top_level = u"test"


class PipeTester(unittest.TestCase):

    def setUp(self):
        self.a = RubberTestClass()
        self.pipe = Pipe([ifilter.Linear(-2), ifilter.linear, ifilter.Linear(3)], lambda cur, dest, speed: cur==3)

    def tearDown(self):
        del self.a
        del self.pipe

    def test_pipe(self):
        self.a.set_filter(u"addition", self.pipe)
        self.a.addition = 4
        self.a.update()
        self.assertEqual(self.a.addition, 2)

    def test_done_condition(self):
        self.a.set_filter(u"addition", self.pipe)
        self.a.addition = 4
        self.a.update()
        self.assertTrue(self.a.is_dirty())
        self.a.update()
        self.assertFalse(self.a.is_dirty())
        self.assertEqual(self.a.addition, 4)

    def test_slicing(self):
        self.a.set_filter(u"addition", self.pipe[1:])
        self.a.addition = 10
        self.a.update()
        self.assertEqual(self.a.addition, 4)


if __name__ == u'__main__':
    unittest.main()