from __future__ import absolute_import
from anime.core.rubberband import RubberBand
import math

class Polar(RubberBand):

    def __init__(self, equation):
        super(Polar, self).__init__()
        self.phi = 0
        self.equation = equation

    def get_x(self):
        u"""Returns in radians"""
        return self.equation(self.phi)*math.cos(self.phi)

    def get_y(self):
        u"""Returns in radians"""
        return self.equation(self.phi)*math.sin(self.phi)

    def get_pos(self):
        return self.get_x(), self.get_y()
