from __future__ import absolute_import
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

setup(
    name=u'anime2',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version=u'1.2.0',

    description=u'Declarative animation library for pygame.',

    # The project's main homepage.
    url=u'https://github.com/SodaCookie/anime',

    # Author details
    author=u'Eric Zhang',
    author_email=u'eric.quin.zhang@gmail.com',

    # Choose your license
    license=u'MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        u'Development Status :: 4 - Beta',

        # Indicate who your project is intended for
        u'Intended Audience :: Developers',
        u'Topic :: Software Development :: Libraries :: pygame',

        # Pick your license as you wish (should match "license" above)
        u'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        u'Programming Language :: Python :: 2',
    ],

    # What does your project relate to?
    keywords=u'pygame animation anime',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=[u'contrib', u'docs', u'tests*']),

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[u'pygame']
)
